<nav class="navbar navbar-fixed-top navbar-default">
    <div class="container">
        <div class="navbar-header">
            <a class="navbar-brand" href="/">OmicX</a>
            <?php if ($template->isLoggedIn()): ?>
            <button id="mobileMenu" type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <?php endif; ?>
        </div>
        <?php if ($template->isLoggedIn()): ?>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li role="separator" class="divider"></li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Menu <span class="caret"></span> </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="/user/">Profile</a>
                        </li>
                        <li>
                            <a href="/tools/">My tools</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <?= $template->getUser() ?> <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="/login/logout">
                                Logout <span class="glyphicon glyphicon-off"></span>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
            <?php endif; ?>
        </div>
    </div>
</nav>