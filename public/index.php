<?php

define('__PUBLIC_DIR__', __DIR__);
define('__ROOT_DIR__', __DIR__ . DIRECTORY_SEPARATOR . '..');
define('__CONF_DIR__', __ROOT_DIR__ . DIRECTORY_SEPARATOR . 'conf');
define('__SRC_DIR__', __ROOT_DIR__ . DIRECTORY_SEPARATOR . 'src');
define('__VIEWS_DIR__', __ROOT_DIR__ . DIRECTORY_SEPARATOR . 'views');

require_once __SRC_DIR__ . '/app.php';