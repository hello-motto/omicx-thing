<?php

namespace Utils;


class ArrayFormatter
{
    /**
     * @param object $object
     * @return array
     */
    public static function convertObjectToArray($object): array
    {
        $returnArray = [];
        foreach (get_class_methods($object) as $method) {
            if (substr($method, 0, 3) === 'get') {
                $parameter = strtolower(substr($method, 3));
                $returnArray[$parameter] = $object->$method();
            }
        }

        return $returnArray;
    }
}