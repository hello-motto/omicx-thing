<?php

namespace Utils;

use Controller\AbstractController;

class Route
{
    /**
     * @return mixed
     */
    public static function dispatchRoute()
    {
        self::checkIfLoggedIn();
        $request = Request::getInstance();
        $controllerParam = empty($request->getQuery('controller')) ? 'login' : $request->getQuery('controller');

        if (!is_null($controllerParam)) {
            $controllerClass = '\Controller\\' . ucfirst($controllerParam) . 'Controller';
            if (class_exists($controllerClass)) {
                $controller = new $controllerClass();
                $variables = $controller->connect();
                return Template::getTemplateVariables($variables);
            }
        }
    }

    /**
     * Check if user is logged in and used controller is the default controller
     */
    public static function checkIfLoggedIn()
    {
        $request = Request::getInstance();

        if (!$request->isLoggedIn() && $request->getQuery('controller') !== AbstractController::DEFAULT_CONTROLLER) {
            self::redirect('/');
        }
    }

    /**
     * @param string $route
     */
    public static function redirect(string $route)
    {
        header('Location: ' . $route);
    }
}