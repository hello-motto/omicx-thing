<?php

namespace Utils;

class Database
{
    /**
     * @var Database|null
     */
    private static $_instance = null;

    /**
     * @var \PDO
     */
    protected static $_connection = null;

    /**
     * Database constructor.
     */
    private function __construct(){}

    /**
     * @return Database
     */
    public static function getInstance()
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new Database();
            if (ConfigReader::checkDatabaseConfig()) {
                $config = ConfigReader::loadDatabaseConfig();
                $dsn = $config['driver'] . ':dbname=' . $config['dbname'] . ';host=' . $config['host'];
                self::$_connection = new \PDO($dsn, $config['user'], $config['password']);
            }
        }

        return self::$_instance;
    }

    /**
     * @return \PDO
     */
    public function getConnection()
    {
        return self::$_connection;
    }
}