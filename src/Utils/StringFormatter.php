<?php

namespace Utils;


class StringFormatter
{
    /**
     * @param string $fieldName
     * @return string
     */
    public static function dataBaseToEntity(string $fieldName): string
    {
        $fieldName = ucwords($fieldName, '_');

        return str_replace('_', '', $fieldName);
    }

    /**
     * @param string $string
     * @return bool
     */
    public static function isEmailValid(string $string): bool
    {
        return filter_var($string, FILTER_VALIDATE_EMAIL);
    }

    /**
     * @param string $string
     * @return string
     */
    public static function getSlug(string $string): string
    {
        $string = strtolower(htmlentities(trim($string), ENT_NOQUOTES, 'UTF-8'));
        $string = preg_replace('#&([A-za-z])(?:acute|caron|cedil|circ|grave|orn|ring|slash|th|tilde|uml);#', '\1', $string);
        $string = preg_replace('#&([A-za-z]{2})(?:lig);#', '\1', $string);
        $string = preg_replace('#&([A-za-z0-9]*);#', '', $string);
        $string = preg_replace('#[ ._:!?%&~)(“”„"‘’\'\^\\\/@$=]#', '-', $string);
        $string = preg_replace('#-{1,}#', '-', $string);
        if(preg_match('#--#', $string))
            return self::getSlug($string);
        else
            return $string;
    }

    /**
     * @param string $mail
     * @param string $password
     * @return string
     */
    public static function encodePassword(string $mail, string $password): string
    {
        return sha1(sha1(strtolower($mail)) . sha1($password));
    }
}