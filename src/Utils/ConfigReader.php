<?php

namespace Utils;

class ConfigReader
{
    /**
     * @return mixed
     * @throws \Exception
     */
    public static function loadConfigFile()
    {
        $configFile = file_get_contents(__CONF_DIR__ . DIRECTORY_SEPARATOR . 'config.json');
        $config = json_decode($configFile, true);

        if (json_last_error() !== 0) {
            throw new \Exception(self::getErrorMessage(json_last_error()), json_last_error());
        }

        return $config;
    }

    /**
     * @return array
     * @throws \Exception
     */
    public static function loadDatabaseConfig()
    {
        $config = self::loadConfigFile();

        if (is_array($config) && array_key_exists('database', $config)) {
            return $config['database'];
        }

        return [];
    }

    /**
     * @return true
     * @throws \Exception
     */
    public static function checkDatabaseConfig()
    {
        $config = self::loadDatabaseConfig();
        $errors = '';

        foreach (self::getNeededParameters() as $parameter) {
            if (!array_key_exists($parameter, $config)) {
                $errors .= "$parameter is needed\r\n";
            }
        }

        if (trim($errors) !== '') {
            throw new \Exception($errors);
        }

        return true;
    }

    /**
     * @return array
     */
    private static function getNeededParameters()
    {
        return ['driver', 'host', 'dbname', 'user', 'password'];
    }

    /**
     * @param integer $code
     * @return string
     */
    private static function getErrorMessage(int $code)
    {
        $errors = [
            JSON_ERROR_NONE => 'No error',
            JSON_ERROR_DEPTH => 'Maximum stack depth exceeded',
            JSON_ERROR_STATE_MISMATCH => 'State mismatch (invalid or malformed JSON)',
            JSON_ERROR_CTRL_CHAR => 'Control character error, possibly incorrectly encoded',
            JSON_ERROR_SYNTAX => 'Syntax error',
            JSON_ERROR_UTF8 => 'Malformed UTF-8 characters, possibly incorrectly encoded',
            'unkown_error' => 'Unknown error.'
        ];

        return array_key_exists($code, $errors) ? 'Config error : ' . $errors[$code] : $errors['unkown_error'];
    }
}