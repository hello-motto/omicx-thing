<?php

namespace DAO;

use Entity\AbstractModel;
use Entity\Tool;

class ToolDAO extends DatabaseDAO
{
    /**
     * @var string
     */
    protected $tableName = 'tool';

    /**
     * @return array
     */
    protected function entityToDatabaseSpecificFields(): array
    {
        return [
            'slug' => ['field' => 'slug', 'nullable' => false, 'index' => 'unique'],
            'label' => ['field' => 'label', 'nullable' => false],
            'description' => ['field' => 'description', 'nullable' => false],
            'user' => ['field' => 'user', 'nullable' => false, 'index' => 'fk']
        ];
    }

    /**
     * @param array $data
     * @param bool $recursive
     * @return AbstractModel
     */
    protected function buildDomainObject(array $data, $recursive = true): AbstractModel
    {
        $tool = new Tool();
        $tool->setId($data['id'])
            ->setCreatedAt(new \DateTime($data['created_at']))
            ->setSlug($data['slug'])
            ->setLabel($data['label'])
            ->setDescription($data['description']);
        if (!is_null($data['updated_at'])) {
            $tool->setUpdatedAt(new \DateTime($data['updated_at']));
        }

        if ($recursive) {
            $userDAO = new UserDAO();
            $user = $userDAO->find($data['user']);
            $tool->setUser($user);
        }

        return $user;
    }
}