<?php

namespace DAO;

use Entity\AbstractModel;
use Entity\User;

class UserDAO extends DatabaseDAO
{
    /**
     * @var string
     */
    protected $tableName = 'user';

    /**
     * @return array
     */
    protected function entityToDatabaseSpecificFields(): array
    {
        return [
            'firstname' => ['field' => 'firstname', 'nullable' => false],
            'lastname' => ['field' => 'lastname', 'nullable' => false],
            'mail' => ['field' => 'mail', 'nullable' => false],
            'password' => ['field' => 'password', 'nullable' => false],
            'description' => ['field' => 'description', 'nullable' => false]
        ];
    }

    /**
     * @param array $data
     * @param bool $recursive
     * @return AbstractModel
     */
    protected function buildDomainObject(array $data, $recursive = true): AbstractModel
    {
        $user = new User();
        $user->setId($data['id'])
             ->setCreatedAt(new \DateTime($data['created_at']))
             ->setFirstName($data['firstname'])
             ->setLastName($data['lastname'])
             ->setDescription($data['description'])
             ->setMail($data['mail'])
             ->setPassword($data['password']);
        if (!is_null($data['updated_at'])) {
            $user->setUpdatedAt(new \DateTime($data['updated_at']));
        }

        if ($recursive) {
            $toolDAO = new ToolDAO();
            foreach ($toolDAO->findBy(['user' => $user->getId()]) as $tool) {
                $user->addTool($tool);
            }
        }

        return $user;
    }
}