<?php

namespace DAO;


use Entity\AbstractModel;
use Utils\Database;

abstract class DatabaseDAO
{
    /**
     * @var \PDO
     */
    protected $connection;

    /**
     * @var string
     */
    protected $tableName;

    /**
     * DatabaseDAO constructor.
     */
    public function __construct()
    {
        $this->connection = Database::getInstance()->getConnection();
    }

    /**
     * @return \PDO
     */
    public function getConnection()
    {
        return $this->connection;
    }

    /**
     * @return array
     */
    public function findAll(): array
    {
        $stmt = $this->connection->query("SELECT * FROM $this->tableName");

        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    /**
     * @param int $id
     * @return AbstractModel
     */
    public function find(int $id): ?AbstractModel
    {
        $stmt = $this->connection->query("SELECT * FROM $this->tableName WHERE id = $id");

        $result = $stmt->fetch(\PDO::FETCH_ASSOC);

        return is_array($result) ? $this->buildDomainObject($result, false) : null;
    }

    /**
     * @param array $criteria
     * @param array $order
     * @return array
     */
    public function findBy(array $criteria, array $orderBy = []): array
    {
        $sql = "SELECT * FROM $this->tableName WHERE ";
        $i = 0;
        foreach ($criteria as $field => $value) {
            $sql .= ($i++ > 0 ? 'AND ' : '') . "$field = ?";
        }

        if (count($orderBy)) {
            $sql . ' ORDER BY ';
            $i = 0;
            foreach ($orderBy as $field => $order) {
                $sql .= ($i++ > 0 ? ', ' : '') . "$field $order";
            }
        }

        $stmt = $this->connection->prepare($sql);

        $stmt->execute(array_values($criteria));

        $datas = [];

        foreach ($stmt->fetchAll(\PDO::FETCH_ASSOC) as $result) {
            $datas[] = $this->buildDomainObject($result, false);
        }

        return $datas;
    }

    /**
     * @param AbstractModel $entity
     * @return bool
     */
    public function save(AbstractModel $entity): bool
    {
        if (is_null($entity->getId())) {
            return $this->insert($entity);
        } else {
            return $this->update($entity);
        }
    }

    /**
     * @param AbstractModel $entity
     * @return bool
     */
    public function insert(AbstractModel $entity): bool
    {
        $fieldsValues = $this->entityValuesToDatabase($entity);
        $params = $this->replaceFieldsNameByQuestionMark($fieldsValues);

        $sql = "INSERT INTO $this->tableName (" . implode(', ', array_keys($fieldsValues)) . ") VALUES ($params)";

        $stmt = $this->connection->prepare($sql);

        return $stmt->execute(array_values($fieldsValues));
    }

    /**
     * @param AbstractModel $entity
     * @return bool
     */
    public function update(AbstractModel $entity): bool
    {

    }

    /**
     * @param array $data
     * @param bool $recursive
     * @return AbstractModel
     */
    protected abstract function buildDomainObject(array $data, $recursive = true): AbstractModel;

    /**
     * @return array
     */
    protected function entityToDatabaseFields(): array
    {
        return array_merge_recursive($this->entityToDatabaseCommonFields(), $this->entityToDatabaseSpecificFields());
    }

    /**
     * @return array
     */
    protected function entityToDatabaseCommonFields(): array
    {
        return [
            'id' => ['field' => 'id', 'nullable' => false, 'index' => 'pk'],
            'createdAt' => ['field' => 'created_at', 'nullable' => false],
            'updatedAt' => ['field' => 'updated_at', 'nullable' => false]
        ];
    }

    /**
     * @return array
     */
    protected abstract function entityToDatabaseSpecificFields(): array;

    /**
     * @return array
     */
    public function entityValuesToDatabase(AbstractModel $entity): array
    {
        $fieldsArray = [];
        foreach ($this->entityToDatabaseFields() as $param => $field) {
            $getter = 'get' . ucfirst($param);
            if (method_exists($entity, $getter)) {
                $value = $entity->{$getter}();
                if ($value instanceof \DateTime) {
                    $value = $value->format('Y-m-d H:i:s');
                } elseif ($value instanceof AbstractModel) {
                    $value = $value->getId();
                } elseif (!is_null($value)) {
                    //$value = "'$value'";
                }
                //if (!is_null($value)) {
                    $fieldsArray[$field['field']] = $value;
                //}
            }
        }

        return $fieldsArray;
    }

    /**
     * @param array $fieldsName
     * @return string
     */
    public function replaceFieldsNameByQuestionMark(array $fieldsName): string
    {
        $params = '';

        for ($i = 0; $i < count($fieldsName); $i++) {
            $params .= ($i > 0 ? ',' : '') . '?';
        }

        return $params;
    }
}