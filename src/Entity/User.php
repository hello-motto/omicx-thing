<?php

namespace Entity;


class User extends AbstractModel
{
    /**
     * @var string
     */
    protected $firstname;

    /**
     * @var string
     */
    protected $lastname;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var string
     */
    protected $mail;

    /**
     * @var string
     */
    protected $password;

    /**
     * @var array<Tool>
     */
    protected $tools = [];

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->getFirstname() . ' ' . $this->getLastname();
    }

    /**
     * @param string $firstname
     * @return User
     */
    public function setFirstname(string $firstname): User
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * @return string
     */
    public function getFirstname(): string
    {
        return $this->firstname;
    }

    /**
     * @param string $lastname
     * @return User
     */
    public function setLastname(string $lastname): User
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * @return string
     */
    public function getLastname(): string
    {
        return $this->lastname;
    }

    /**
     * @param string $description
     * @return User
     */
    public function setDescription(string $description): User
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $mail
     * @return User
     */
    public function setMail(string $mail): User
    {
        $this->mail = $mail;

        return $this;
    }

    /**
     * @return string
     */
    public function getMail(): string
    {
        return $this->mail;
    }

    /**
     * @param string $password
     * @return User
     */
    public function setPassword(string $password): User
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param array $tools
     * @return User
     */
    public function setTools(array $tools): User
    {
        $this->removeAllTools();
        foreach ($tools as $tool) {
            if ($tool instanceof Tool) {
                $this->tools[$tool->getId()] = $tool;
            }
        }

        return $this;
    }

    /**
     * @return User
     */
    public function removeAllTools(): User
    {
        $this->tools = [];

        return $this;
    }

    /**
     * @param Tool $tool
     * @return User
     */
    public function addTool(Tool $tool): User
    {
        $this->tools[$tool->getId()] = $tool;

        return $this;
    }

    /**
     * @param Tool $tool
     * @return User
     */
    public function removeTool(Tool $tool): User
    {
        $tools = $this->getTools();
        if (array_key_exists($tool->getId(), $tools)) {
            unset($tools[$tool->getId()]);
        }

        return $this;
    }

    /**
     * @return array<Tool>
     */
    public function getTools(): array
    {
        return $this->tools;
    }

    /**
     * @return string
     */
    public function getDAOClass(): string
    {
        return '\\DAO\\UserDAO';
    }
}