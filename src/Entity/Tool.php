<?php

namespace Entity;


class Tool extends AbstractModel
{
    /**
     * @var string
     */
    protected $slug;

    /**
     * @var string
     */
    protected $label;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var User
     */
    protected $user;

    /**
     * @param string $label
     * @return Tool
     */
    public function setLabel(string $label): Tool
    {
        $this->label = $label;

        return $this;
    }

    /**
     * @return string
     */
    public function getLabel(): string
    {
        return $this->label;
    }

    /**
     * @param string $slug
     * @return Tool
     */
    public function setSlug(string $slug): Tool
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @param string $description
     * @return Tool
     */
    public function setDescription(string $description): Tool
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param User $user
     * @return Tool
     */
    public function setUser(User $user): Tool
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @return string
     */
    public function getDAOClass(): string
    {
        return '\\DAO\\ToolDAO';
    }
}