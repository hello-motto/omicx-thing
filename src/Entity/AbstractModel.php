<?php

namespace Entity;

use DAO\DatabaseDAO;

abstract class AbstractModel
{
    /**
     * @var integer
     */
    protected $id;

    /**
     * @var \DateTime
     */
    protected $createdAt;

    /**
     * @var \DateTime
     */
    protected $updatedAt;

    public function __construct()
    {
        $this->setCreatedAt(new \DateTime());
    }

    /**
     * @param int $id
     * @return AbstractModel
     */
    final public function setId(int $id): AbstractModel
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return int
     */
    final public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param \DateTime $createdAt
     * @return AbstractModel
     */
    final public function setCreatedAt(\DateTime $createdAt): AbstractModel
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return \DateTime
     */
    final public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $updatedAt
     * @return AbstractModel
     */
    final public function setUpdatedAt(\DateTime $updatedAt): AbstractModel
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return \DateTime
     */
    final public function getUpdatedAt(): ?\DateTime
    {
        return $this->updatedAt;
    }

    /**
     * Return the class name that contains methods for the Database
     *
     * @return string
     */
    abstract public function getDAOClass(): string;

    /**
     * Return the class that contains methods for the Database
     *
     * @return DatabaseDAO
     */
    public function getDAO(): DatabaseDAO
    {
        $daoClass = $this->getDAOClass();

        if (class_exists($daoClass)) {
            return new $daoClass();
        }

        return null;
    }
}