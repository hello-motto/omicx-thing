<?php

namespace Controller;


use Entity\User;
use Utils\ArrayFormatter;

class UserController extends AbstractController
{
    /**
     * @return array
     * @throws \Exception
     */
    public function homeAction(): array
    {
        /** @var User $user */
        $user = $this->getRequest()->getSession('user');

        $form = ArrayFormatter::convertObjectToArray($user);
        $formErrors = $successMessage = $errorMessage = '';
        return [
            'title' => 'Welcome ' . $user,
            'form' => $form,
            'formErrors' => $formErrors,
            'successMessage' => $successMessage,
            'errorMessage' => $errorMessage,
            'template' => $this->getTemplateName()
        ];
    }
}