<?php

namespace Controller;


use Entity\User;
use Utils\Request;

abstract class AbstractController
{
    /**
     * @var string
     */
    const DEFAULT_CONTROLLER = 'login';

    /**
     * @var string
     */
    const DEFAULT_METHOD = 'home';

    /**
     * @return \Utils\Request
     */
    public function getRequest()
    {
        return Request::getInstance();
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function connect(): array
    {
        $request = $this->getRequest();

        $method = $request->getQuery('method') . 'Action';

        if (method_exists($this, $method)) {
            return $this->$method();
        }

        throw new \Exception("Method '$method' not found in class '".get_class($this)."'.");
    }

    /**
     * @param string $type
     * @return string
     * @throws \Exception
     */
    protected function getTemplateName($type = 'html'): string
    {
        $request = $this->getRequest();

        $file = __VIEWS_DIR__ . DIRECTORY_SEPARATOR . $request->getQuery('controller') . DIRECTORY_SEPARATOR
            . $request->getQuery('method') . '.' . $type . '.php';

        if (file_exists($file)) {
            return $file;
        }

        throw new \Exception("Template '$file' not found.");
    }

    /**
     * @return \Entity\User|null
     */
    public static function getUser()
    {
        $request = self::getRequest();

        return $request->getSession('user');
    }

    /**
     * @return array
     */
    public abstract function homeAction(): array;
}